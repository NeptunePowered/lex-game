package uk.jamierocks.lexteam.ygd.core.object.tool.connection;

/**
 * An enum representing the different frequencies of connection.
 * Currently just different colours.
 *
 * @author Tom Drever
 * @author Jamie Mansfield
 */
public enum ConnectionFrequency {

    /**
     * Represents a 'red' connection
     */
    RED,

    /**
     * Represents a 'blue' connection
     */
    BLUE,

    /**
     * Represents a 'green' connection
     */
    GREEN,

    /**
     * Represents a 'orange' connection
     */
    ORANGE,

    /**
     * Represents a 'purple' connection
     */
    PURPLE
}
