/**
 * The package includes all the files regarding sections
 *
 * Sections are basically collections of {@link uk.jamierocks.lexteam.ygd.core.level.Level}s.
 */
package uk.jamierocks.lexteam.ygd.core.section;