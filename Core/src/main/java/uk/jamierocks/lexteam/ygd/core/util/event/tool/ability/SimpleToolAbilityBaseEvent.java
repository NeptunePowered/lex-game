package uk.jamierocks.lexteam.ygd.core.util.event.tool.ability;

import uk.jamierocks.lexteam.ygd.core.object.tool.ToolAbility;

/**
 * Created by Jamie_2 on 16/05/2015.
 */
public class SimpleToolAbilityBaseEvent extends ToolAbilityBaseEvent {

    public SimpleToolAbilityBaseEvent(ToolAbility ability) {
        super(ability);
    }
}
