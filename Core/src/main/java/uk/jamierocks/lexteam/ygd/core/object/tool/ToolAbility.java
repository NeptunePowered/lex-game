package uk.jamierocks.lexteam.ygd.core.object.tool;

/**
 * The enum of tool abilities
 *
 * @author Jamie Mansfield
 */
public enum ToolAbility {

    CHANGE_CONNECTION_FREQUENCY,

    CREATE_POINT,

    MERGE_CONNECTION,

    REMOVE_POINT,

    REVERSE_CONNECTION
}
