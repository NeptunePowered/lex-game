package uk.jamierocks.lexteam.ygd.core.util.event.tool.ability;

import uk.jamierocks.lexteam.ygd.core.object.tool.ToolAbility;

/**
 * Created by Tom
 */
public class ConnectionFrequencyToolAbilityBaseEvent extends ToolAbilityBaseEvent {

    public ConnectionFrequencyToolAbilityBaseEvent(ToolAbility ability) {
        super(ability);
    }
}
