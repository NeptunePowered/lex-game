package uk.jamierocks.lexteam.ygd.core.task;

/**
 * Represents an owner of a task
 *
 * @author Jamie Mansfield
 */
public interface TaskOwner {

}
