package uk.jamierocks.lexteam.ygd.core.object;

/**
 * Represents any object within the game
 *
 * @author Tom Drever
 * @author Jamie Mansfield
 */
public interface GameObject {

}
