package uk.jamierocks.lexteam.ygd.enginelib.interfaces.scene;

public interface BaseModel {

    boolean setPos(float x, float y, float z);

    boolean setPos(Tile tilepos);
}
