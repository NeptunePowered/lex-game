package uk.jamierocks.lexteam.ygd.enginelib.interfaces.gui;

/**
 * Represents an in-game {@link Button}, that can be pushed.
 *
 * @author Jamie Mansfield
 */
public interface PushButton extends Button {

}
