package uk.jamierocks.lexteam.ygd.enginelib.interfaces.scene;

public interface Transformable {

    boolean setRotation(float x, float y, float z);

    boolean setScale(Tile scale);
}
