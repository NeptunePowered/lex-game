#ifndef _LEVEL_HPP
#define _LEVEL_HPP

#include "..\..\level\headers\LevelDesign.h"
#include "..\..\level\headers\LevelGenrator.h"
#include "..\..\level\headers\LevelScene.h"
#include "..\..\level\headers\TileManager.h"
#include "..\..\level\Puzzle\headers\Puzzle.h"

#endif //_LEVEL_HPP